package controllers;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.*;
import play.libs.Json;
import play.mvc.*;

public class Application extends Controller {
    
    public static Result test() {
    	//JsonNode json = request().body().asJson();
    	ObjectNode result = Json.newObject();
    	result.put("foo", "bar");
    	result.put("soup", "io");
    	return ok(result);
    }
    
    public static Result greet() {
    	Map<String, String[]> queryParams = request().queryString();
    	String name = "World";
    	if (queryParams.containsKey("name")) {
    		name = queryParams.get("name")[0];
    	}
    	ObjectNode result = Json.newObject();
    	result.put("result", "Hello " + name + "!");
    	return ok(result);
    }
    
    public static Result greet2(String name) {
    	ObjectNode result = Json.newObject();
    	result.put("result", "Welcome, " + name + "!");
    	return ok(result);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result post() {
    	JsonNode json = request().body().asJson();
    	ObjectNode result = Json.newObject();
    	String name = json.findPath("name").textValue();
    	result.put("result", "Body of proof that " + name + " exists!");
    	return ok(result);
    }
}
